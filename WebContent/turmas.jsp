<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Aplica��o Turmas</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/turmas.css" rel="stylesheet">
</head>

<body>

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="http://localhost:8080/turmasv1">Home</a>
			</div>
		</div>
	</div>
	
	<div class="jumbotron">
			<h1>Turmas</h1>
			<p>Cadastro de turma e alunos</p>
		</div>

	<div class="container">

		<div class="main-page">
			<h1 class="titulo">Turmas</h1>

			<div class="panel panel-primary"">
				<div class="panel-heading">Cadastrar Nova Turma</div>
				<div class="panel-body">
					<form action="addturma.do" method="POST" class="form-horizontal"
						role="form">

						<div class="form-group">
							<label for="nome" class="col-sm-2 control-label">Nome da
								turma:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="nome" name="nome"
									placeholder="Nome"/>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">Criar</button>
							</div>
						</div>

					</form>

				</div>
			</div>

			<c:if test="${fn:length(turmas) > 0}">

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Turma</th>
							<th>Criada em</th>
							<th>Alunos matriculados</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="turma" items="${turmas}">
							<tr>
								<td><a href="editturma.do?idTurma=${turma.id}">${turma.nome}</a>
								</td>
								<td><fmt:formatDate type="date"
										value="${turma.dataCriacao}" /></td>
								<td>${fn:length(turma.alunos)}</td>
								<td><a href="delturma.do?idTurma=${turma.id}" class="btn btn-primary btn-sm active" role="button"><span
										class="glyphicon glyphicon-trash"></span> Delete</a>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</c:if>

		</div>

	</div>
	<!-- /.container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>