<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Aplica��o Turmas</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/turmas.css" rel="stylesheet">
</head>

<body>

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
			
				<a class="navbar-brand" href="http://localhost:8080/turmasv1">Home</a>
			</div>
		</div>
	</div>

	<div class="container">
		
		<div class="main-page">
			<a href="listaturma.do" class="btn btn-primary">Voltar</a>

			<h1 class="titulo">Turmas</h1>

			<h3>${turma.nome}</h3>

			<br/>

			<div class="panel panel-primary"">
				<div class="panel-heading">Matricular novo aluno</div>
				<div class="panel-body">
					<form action="addaluno.do" method="POST" class="form-horizontal">
						<input type="hidden" name="idTurma" value="${turma.id}">
						<div class="form-group">

							<label for="nome" class="col-sm-2 control-label">Nome:</label>
							<div class="col-sm-10">
								<input name="nome" type="text" class="form-control"
									placeholder="Nome" />

							</div>

							<label for="nome" class="col-sm-2 control-label">Matr�cula:</label>
							<div class="col-sm-10">
								<input name="matricula" class="form-control" type="text"
									placeholder="Matr�cula" />
							</div>

							<label for="nome" class="col-sm-2 control-label">Email:</label>
							<div class="col-sm-10">
								<input name="email" class="form-control" type="email"
									placeholder="Email" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn btn-default" value="Matricular">
							</div>
						</div>
					</form>

				</div>
			</div>

			<c:if test="${not empty turma.alunos}">

				<h3>Alunos matriculados</h3>

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Matr�cula</th>
							<th>Nome do aluno</th>
							<th>Email</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="aluno" items="${turma.alunos}">
							<tr class="dados">
								<td>${aluno.matricula}</td>
								<td>${aluno.nome}</td>
								<td>${aluno.email}</td>
								<td><a href="delaluno.do?idAluno=${aluno.id}" class="btn btn-primary btn-sm active" role="button"><span
										class="glyphicon glyphicon-trash"></span> Delete</a>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:if>

		</div>

	</div>
	<!-- /.container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>