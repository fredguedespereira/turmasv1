# Projeto Eclipse: TurmasWebApp-v1 #

Projeto de turmas e alunos matriculados.

### Que projeto é este? ###

* Um pequeno sistema em Java servlets para criar turmas e matricular alunos nestas turmas.
* Versão 1.0
* [Sintaxe Markdown deste Readme](https://bitbucket.org/tutorials/markdowndemo)

### Instruções de configuração e execução ###

* Importe o arquivo no Eclipse a partir de File > Import... > Existing projects into workspace > Select archive file: TurmasWebApp-v1.zip;
* Ajuste o build path do projeto no seu computador, pode ser que a sua versão de Java ou do Tomcat não sejam o mesmo dos que foram usados neste projeto. Se houver um erro de build path no projeto, o Eclipse põe uma exclamação vermelha no ícone dele. Mesmo que não esteja com ela, verifique o build path  e faça os ajustes necessários (indicar a sua máquina virtual Java, seu Tomcat, suas bibliotecas etc.).
* Baixe o Hibernate e descompacte em c:\java\hibernate (sugestão);
* Baixe o SGBD HSQLDB e descompacte em c:\java\hsqldb;
* Crie uma biblioteca do usuário chamada JPA no Eclipse, adicione os arquivos em c:\java\hibernate\lib\jpa\*.jar, c:\java\hibernate\lib\required\*.jar e o c:\java\hsqldb\lib\hsqldb.jar;
* Adicione esta biblioteca do usuário ao build path do projeto (ela já deve existir; se for o caso, apenas edite-a para adicionar os jar´s acima);
* Crie o diretório c:\java\db\turmasweb;
* Execute o SGBD HSQLDB numa janela de comandos no diretório c:\java\hsqldb: 
   **java -cp .\lib\hsqldb.jar org.hsqldb.server.Server --database.0 file:c:\java\db\turmasweb\turmasweb --dbname.0 tweb**
* Para evitar ficar digitando a linha acima sempre que for iniciar este SGBD, recomendo criar um arquivo de script DOS chamado "starttweb.bat" e digitar o conteúdo acima dentro do arquivo. Agora, basta clicar duas vezes no arquivo.
* Implante o projeto no Tomcat e inicie o container;
* Digite num navegador: http://localhost:8080/turmasv1.

### Autor do Projeto ###

* Fred Guedes Pereira
