package br.edu.ifpb.pweb.turmas.servlet;

import java.io.IOException;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.pweb.turmas.dao.PersistenceUtil;
import br.edu.ifpb.pweb.turmas.dao.TurmaDAO;
import br.edu.ifpb.pweb.turmas.model.Turma;

/**
 * Servlet implementation class for Servlet: AddTurmaServlet
 * 
 */

@WebServlet(value="/addturma.do")
public class AddTurmaServlet extends javax.servlet.http.HttpServlet implements
		javax.servlet.Servlet {

	private void doRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// Pega o valor do campo "nome" do formul�rio HTML e cria uma nova turma com este nome
		String nome = request.getParameter("nome");		
		Turma turma = new Turma();
		turma.setNome(nome);
		turma.setDataCriacao(new Date());
		
		// Cria o TurmaDAO e abre uma transa��o para inserir a nova turma no BD
		EntityManagerFactory emf = (EntityManagerFactory) request.getServletContext().getAttribute("emf");
		EntityManager em = emf.createEntityManager();
		TurmaDAO tDAO = new TurmaDAO(em);
		tDAO.beginTransaction();
		tDAO.insert(turma);
		tDAO.commit();
		
		// Repassa a requisi��o para a o servlet que lista as turmas
		RequestDispatcher d = request.getRequestDispatcher("listaturma.do");
		d.forward(request, response);
		
		em.close();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doRequest(request, response);
	}
}