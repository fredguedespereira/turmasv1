package br.edu.ifpb.pweb.turmas.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.pweb.turmas.dao.AlunoDAO;
import br.edu.ifpb.pweb.turmas.dao.PersistenceUtil;
import br.edu.ifpb.pweb.turmas.dao.TurmaDAO;
import br.edu.ifpb.pweb.turmas.model.Aluno;
import br.edu.ifpb.pweb.turmas.model.Turma;

/**
 * Servlet implementation class for Servlet: AddAlunoServlet
 *
 */
@WebServlet(value="/delaluno.do")
 public class DelAlunoServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
		private void doRequest(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException{
			// Pega parametros do aluno dos campos do formul�rio HTML
			Long idAluno  = Long.valueOf(request.getParameter("idAluno"));
			
			// Recupera o aluno a ser exclu�do do banco
			EntityManagerFactory emf = (EntityManagerFactory) request.getServletContext().getAttribute("emf");
			EntityManager em = emf.createEntityManager();
			AlunoDAO aDAO = new AlunoDAO(em);
			Aluno delAluno = aDAO.find(idAluno);
			TurmaDAO tDAO = new TurmaDAO(em);
			long turmaId = delAluno.getTurma().getId();
			Turma turma = tDAO.find(turmaId);
			
			// Exclui aluno do banco
			aDAO.beginTransaction();
			turma.getAlunos().remove(delAluno);
			aDAO.commit();
			
			// Repassa a requisi��o HTTP para o servlet que lista a turma espec�fica
			RequestDispatcher d = request.getRequestDispatcher("editturma.do?idTurma="+turmaId);
			d.forward(request, response);
			
			em.close();

		}
	 
	 protected void doGet(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {
			doRequest(request, response);
		}

		protected void doPost(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {
			doRequest(request, response);
		}   
}