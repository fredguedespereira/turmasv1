package br.edu.ifpb.pweb.turmas.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.pweb.turmas.dao.PersistenceUtil;
import br.edu.ifpb.pweb.turmas.dao.TurmaDAO;
import br.edu.ifpb.pweb.turmas.model.Aluno;
import br.edu.ifpb.pweb.turmas.model.Turma;

/**
 * Servlet implementation class for Servlet: AddAlunoServlet
 *
 */
@WebServlet(value="/addaluno.do")
 public class AddAlunoServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
		private void doRequest(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException{
			// Pega parametros do aluno dos campos do formul�rio HTML
			Long idTurma = Long.valueOf(request.getParameter("idTurma"));
			String nome = request.getParameter("nome");
			String matricula = request.getParameter("matricula");
			String email = request.getParameter("email");
			
			// Instancia um aluno com os dados do formulario
			Aluno novoAluno = new Aluno();
			novoAluno.setNome(nome);
			novoAluno.setMatricula(matricula);
			novoAluno.setEmail(email);
			
			// Pega a EntityManager 
			EntityManagerFactory emf = (EntityManagerFactory) request.getServletContext().getAttribute("emf");
			EntityManager em = emf.createEntityManager();			
			
			// Reatacha a turma � EntityManager, pegando-a novamente do banco
			TurmaDAO tDAO = new TurmaDAO(em);
			Turma turma = tDAO.find(idTurma);
			
			// Adiciona o novo aluno � turma
			tDAO.beginTransaction();
			turma.addAluno(novoAluno);
			tDAO.commit();
			
			request.setAttribute("turma", turma);
			
			// Repassa a requisi��o HTTP para o servlet que lista a turma espec�fica
			RequestDispatcher d = request.getRequestDispatcher("editturma.do");
			d.forward(request, response);
			
			em.close();

		}
	 
	 protected void doGet(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {
			doRequest(request, response);
		}

		protected void doPost(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {
			doRequest(request, response);
		}   
}