package br.edu.ifpb.pweb.turmas.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.pweb.turmas.dao.TurmaDAO;
import br.edu.ifpb.pweb.turmas.model.Turma;

/**
 * Servlet implementation class for Servlet: EditTurmaServlet
 * 
 */
@WebServlet(value = "/editturma.do")
public class EditTurmaServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Pega o campo oculto (hidden) do formul�rio HTML deste turma
		Long idTurma = Long.valueOf(request.getParameter("idTurma"));

		// Cria o DAO para pegar a turma escolhida
		EntityManagerFactory emf = (EntityManagerFactory) request.getServletContext().getAttribute("emf");
		EntityManager em = emf.createEntityManager();
		TurmaDAO dao = new TurmaDAO(em);
		Turma turma = dao.find(idTurma); //dao.getTurmaById(idTurma);

		// Poe o objeto turma na sess�o HTTP
		request.setAttribute("turma", turma);
		
		em.close();

		// Repassa a requisi��o HTTP para a p�gina JSP que exibe a turma
		RequestDispatcher d = request.getRequestDispatcher("turma.jsp");
		d.forward(request, response);
		
		
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doRequest(request, response);
	}
}