package br.edu.ifpb.pweb.turmas.servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;

import br.edu.ifpb.pweb.turmas.dao.PersistenceUtil;
import br.edu.ifpb.pweb.turmas.dao.TurmaDAO;
import br.edu.ifpb.pweb.turmas.model.Turma;

@WebServlet(value = "/listaturma.do")
public class ListaTurmasServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static Logger logger = Logger.getLogger(ListaTurmasServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Pega a lista de turmas no banco de dados
		EntityManagerFactory emf = (EntityManagerFactory) request.getServletContext().getAttribute("emf");
		EntityManager em = emf.createEntityManager();
		List<Turma> turmas = new TurmaDAO(em).findAll();
		
		// Põe a lista de turma como atributo do objeto HttpServletRequest
		request.setAttribute("turmas", turmas);
		
		//Fecha o EntityManager
		// Atencao, sem o filtro de conversações, esta linha produzir� o LazyInitializationException na vis�o!
		//em.close();
		
		// Repassa a requisição HTTP para a página JSP
		RequestDispatcher d = request.getRequestDispatcher("turmas.jsp");
		d.forward(request, response);
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
}