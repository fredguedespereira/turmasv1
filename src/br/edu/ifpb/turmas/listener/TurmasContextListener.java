package br.edu.ifpb.turmas.listener;

import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import br.edu.ifpb.pweb.turmas.dao.PersistenceUtil;

@WebListener
public class TurmasContextListener implements ServletContextListener {

	private EntityManagerFactory emf;

	public void contextDestroyed(ServletContextEvent e) {
		if (emf != null && emf.isOpen()) {
			emf.close();
		}
	}

	public void contextInitialized(ServletContextEvent e) {
		emf = PersistenceUtil.getEntityManagerFactory();
		e.getServletContext().setAttribute("emf", emf);
	}

}
